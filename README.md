(EN) From Sunday 19th till Monday 27th of November, OSP is proposing another metapost workshop at Port-au-Prince, Haïti. A workshop with the proposal to draw together a parametric font using Metafont-Metapost. All participants are working on the same etherpad in a browser, so no software install is needed. Everyone can begin to draw after a few minutes when knowing only the first of the large amount of mathematical commands provided by Metafont-Metapost. After a few hours, each participant can copy parts of code from other, so the learning curve is quick, and emulating. Because some part of the code is shared, the feeling of a community drawing the same font, even with a large autonomy for each glyph, is palpable. And it provide a good way to avoid the nerdy ambiance of many workshops based on coding on a solitary way. Technically, an etherpad server could run on a Raspberry Pi on the wifi network (no need for an internet connection), one script is producing previews of each glyph on a common beamed webpage, and another script allows to plot each drawing on a pen plotter machine when a participant want to keep a specific stage.

(FR) Du 19 au 27 novembre, OSP est à Haiti pour un workshop metapost à l'invitation croisée de trois écoles d'art, l'Enarts, La Cambre et le Septante-Cinq. Le workshop s'appuie sur Metafont-Metapost, un langage de programmation libre et open source qui permet de créer des polices de caractère de manière ludique et intuitive. À l’opposé des principaux logiciels de dessins de lettres qui, s’inscrivant dans l’héritage de Gutenberg, modélisent celles-ci par leur contour, Metafont-Metapost décrit la lettre par son “ductus”, c’est-à-dire son chemin central ou squelette. Repartant du geste calligraphique, cet outil propose une conception digitale et paramétrique du caractère typographique. L’action se concentre sur le dessin collectif d’un ensemble de caractères, qui peuvetn aussi être des symboles et pictogrammes, à travers un éditeur collaboratif permettant aux participants de travailler en temps réel sur une même famille de caractères. En parallèle de cette écriture collective, une vue “image” synchronisée et projetée des caractères permettra de voir les dessins s’actualiser en temps réel et réagissant aux changements apportés par les participants. Les résultats seront tracés par des tables traçantes au fil de l’atelier, rendant tangible, autrement qu'à travers la black box opaque des imprimantes, les mouvements vectoriels au travail.

Pour des étudiants en arts qui ne seraient pas directement intéressés par la typographie, la proposition est de comprendre que les éléments d'écriture et de dessin à l'œuvre à travers le code collaboratif réfèrent aussi à n'importe quelle pratique plastique, d'une manière différente, et grâce au rapport visuel direct, permettre un accès radical à la computation informatique. L'idée est aussi de nous connecter, tous, à ce qui est à l'oeuvre dans les nombreux ateliers de lettrage et de découpe vinyle de l'île. C'est un workshop hautement spéculatif et la bifurcation large en fait partie. C'est clairement aussi cette résilience et cette découverte qui peut susciter des avancées pédagogiques, conceptuelles, et -risqons-nous aux grands mots, émancipatoires.

Programme planifié [et tel que déroulé entre crochets] :

* dimanche soir : retrouvailles avec Olrich Exantus à l'aéroport, puis rencontre avec Judith Michel [du Centre d'Art et de Philippe Dodard, directeur de l'ENARTS]
* lundi 9h : installation du workshop et rencontres, entre autres Jean Baptiste Marc (Logipam) [le mercredi]
* lundi 10h : accueil des participants
* lundi 17h : projection à FoKal ou au Centre d'Art, ouverte à tous [prévue pour le samedi, remplacée par le rendu du workshop]
* mardi 9h : workshop
* mardi 10h : visite de Jean Mathiot, directeur de l'Institut français [le lundi]
* mardi 11h : visite du Ministre-Président de la Fédération Wallonie-Bruxelles Rudy Demotte [avec Fabrice Sprimont, dautre représentant du WBI et l'ambassadeur de Belgique en Haïti]
* mardi 16h : projection et représentation du Festival 4 Chemins
* mardi 19h : réception au Consulat de Belgique [en fait à la résidence du Consul honoraire, nombreuses rencontres]
* mercredi 9h : workshop
* jeudi 9h : workshop
* jeudi soir : soirée vaudou à Croix des Bouquets
* vendredi 9h : workshop
* samedi 9h : workshop et installation mini-expo
* samedi 14h : rendu public du workshop et rencontres
* dimanche : visite aux sculpteurs de la Grand Rue [dont le lieu de la Ghetto Bienniale]

Et en plus, idéalement, des rencontres avec Diderot Musset (Surtab), la communauté OSM Haïti, la Ghetto Bienniale [qui n'ont pas pu se goupiller en final].