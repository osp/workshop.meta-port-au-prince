#!/bin/bash
SPIROU=('F')
DOSSIER='A'
tLen=${#SPIROU[@]}
Format='png '
echo ${SPIROU[*]}

for ((i=1; i<35; i++ ))
do 
	CHEMIN='frames/'
	VarPost=`echo "scale=2; 1 - $i / 100" | bc -l`
	sed -r 's/^val:=.*$/val:='$VarPost';/g' paramSave.mp >> param.mp
	mpost -interaction=batchmode -s 'outputformat="svg"' edek.mp
	mv edek-70.svg svg/${SPIROU[$a]}.svg
	convert svg/${SPIROU[$a]}.svg $CHEMIN/${SPIROU[$a]}-$1-$VarPost.png
	Final+=$CHEMIN${SPIROU[$j]}-$1-$VarPost.$Format
	montage ${Final[*]} -mode Concatenate  -tile x1 -geometry +2+2 $CHEMIN/Anim-$1-$VarPost.png
 
#    for (( a=0; a<${tLen}; a++ ));
#           do
#	    mpost -interaction=batchmode -s 'outputformat="svg"' edek.mp
#	    mv edek-70.svg svg/${SPIROU[$a]}.svg
#	    convert svg/${SPIROU[$a]}.svg $CHEMIN/${SPIROU[$a]}-$1-$VarPost.png
#	done
#    echo '%TempFile' > param.mp
#    Final=''
#    for ((j=0;j<${tLen}; j++ ));
#    do
#	Final+=$CHEMIN${SPIROU[$j]}-$1-$VarPost.$Format
#    done
#    montage ${Final[*]} -mode Concatenate  -tile x1 -geometry +2+2 $CHEMIN/Anim-$1-$VarPost.png
    
done
mkdir $CHEMIN/final

cp $CHEMIN/Anim-$1-*.png $CHEMIN/final

mogrify -resize x500 $CHEMIN/final/*.png

convert -size 564x500 xc:white +swap -gravity center -delay 0 \
	               $CHEMIN/final/Anim-$1-*.png  $CHEMIN/final/Anim-$1.gif

convert  $CHEMIN/final/Anim-$1.gif -resize x400 $CHEMIN/final/Anim-$1-resize.gif

rm *.log
