#!/bin/bash

# stop other instance if its running by accident
bash stop.sh

if [ $# -gt 0 ]; then
        padname=$1
else
        padname="metafont"
fi

echo "Starting listener. Listening to to pad '$padname'"

# Starting screen process
screen -dmS mpost bash exportPad.sh $padname

screen -dmS api bash plotter/start_api.sh

screen -dmS plotter bash plotter/start_runner.sh

sudo service nginx reload
