padname=$1
baseurl="http://pads.osp.kitchen"

for (( ; ;  ));
do
  echo ""
  echo ""
  echo $padname
  mkdir -p "svg"

  echo 'outputtemplate := "svg/%c.svg";' > "pad.mp"
  curl "${baseurl}/p/${padname}/export/txt" >> "pad.mp"
  rm -f "svg/*.svg"
  mpost -interaction=batchmode -s 'outputformat="svg"' "pad.mp"
  
  sleep 10
done
