# Synchronise svg
#rsync -avzx --progress --stats --delete pi@192.168.0.100:/home/pi/Documents/osp.workshop.metahaiti/svg .

# generate pdf with html2print
wkhtmltopdf --orientation Portrait --page-size A7 html2print.tiny/index.html --javascript-delay 4000 in.pdf

# Impose the pdf
podofoimpose source.list out.pdf in32.plan

pdftk out.pdf cat 1 output out_1.pdf
pdftk out.pdf cat 2 output out_2.pdf

# Convert to SVG
inkscape out_1.pdf -l out_1.svg
inkscape out_2.pdf -l out_2.svg


vim out_1.svg -c "norm gg32@a" -c ":wq"
vim out_2.svg -c "norm gg32@a" -c ":wq"

# convert to HPGL
/usr/share/inkscape/extensions/hpgl_output.py --autoAlign=FALSE --orientation=0 out_1.svg > out_1.hpgl
/usr/share/inkscape/extensions/hpgl_output.py --autoAlign=FALSE --orientation=0 out_2.svg > out_2.hpgl

# Plot it
